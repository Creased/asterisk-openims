Interconnexion de Asterisk et OpenIMS
=====================================

## Informations ##

### Arborescence ###

    .                   # Fiches d'installation et de configuration étape par étape des différents services
    ├─── captures       # Captures de trames (PCAP)
    ├─── illustrations  # Schémas et captures d'écran
    └─── scripts        # Scripts d'installation "one-shot" des différents services

## Mise en route de Asterisk et OpenIMS ##

Pour une installation étape par étape, se réferer aux fiches&nbsp;:

- [installation_openims.pdf](installation_openims.pdf)&nbsp;: Installation de OpenIMS;
- [installation_asterisk.pdf](installation_asterisk.pdf)&nbsp;: Installation de Asterisk A et Asterisk B.

Pour une installation "one-shot"&nbsp;:

- OpenIMS&nbsp;:

    ```bash
    sudo su -
    ./openims_install.sh
    ```

- Asterisk A&nbsp;:

    ```bash
    sudo su -
    ./asterisk-a_install.sh
    ```

- Asterisk B (cloné depuis Asterisk A)&nbsp;:

    ```bash
    sudo su -
    ./asterisk-b_install.sh
    ```

## Redémarrage des services IMS ##


```bash
sudo su -
./reload_ims.sh
```

## Connexion des softphones ##

### Connexion d'un softphone sur Asterisk A ###

![Client X-Lite sur Asterisk A](illustrations/screenshots/jdoe_xlite_asterisk_a.png)

### Connexion d'un softphone sur Asterisk B ###

![Client X-Lite sur Asterisk B](illustrations/screenshots/jsmith_xlite_asterisk_b.png)
