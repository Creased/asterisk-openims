#!/usr/bin/env bash
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

function print() {
    if (( $# == 1 )); then
        str=${1}
        printf "%b" "${str}\n" >&3
    fi
}

exec 3>/dev/stdout
exec 2>&1
exec 1>/dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

export HOSTNAME=asterisk-b
export DOMAIN=open-ims.test
export IP=192.168.88.226
export MASK=255.255.248.0
export GW=192.168.93.254
export DNS=192.168.92.6
export DEBIAN_FRONTEND=noninteractive
export IP_IMS=192.168.88.115

print "
# HOSTNAME:\t\t${HOSTNAME}\t\t#
# DOMAIN:\t\t${DOMAIN}\t\t#
# IP:\t\t\t${IP}\t\t#
# MASK:\t\t\t${MASK}\t\t#
# GW:\t\t\t${GW}\t\t#
# DNS:\t\t\t${DNS}\t\t#
# IP_IMS:\t\t${IP_IMS}\t\t#
# DEBIAN_FRONTEND:\t${DEBIAN_FRONTEND}\t\t#
"

print "[+] Configuration réseau"

cat <<-EOF >/etc/network/interfaces
##
# Default configuration
#
iface default inet dhcp

##
# Loopback NIC configuration
#
auto lo
iface lo inet loopback

##
# Ethernet NIC configuration
#
auto eth0
allow-hotplug eth0
iface eth0 inet static
    address ${IP}
    netmask ${MASK}
    gateway ${GW}
    dns-search ${DOMAIN}
    dns-nameservers ${IP_IMS} ${DNS}

EOF

ifdown eth0; ifup eth0

print "[+] Configuration du nom d'hôte"

cat <<EOF >/etc/hostname
${HOSTNAME}
EOF

cat <<EOF >/etc/hosts
# IPv4
127.0.0.1 localhost
127.0.1.1 ${HOSTNAME}

# IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

hostname ${HOSTNAME}
hostname -b ${HOSTNAME}
hostnamectl set-hostname ${HOSTNAME}
sysctl kernel.hostname

print "[+] Configuration de la résolution de noms"

cat <<-EOF >/etc/resolv.conf
search open-ims.test
domain open-ims.test
nameserver ${IP_IMS}
nameserver ${DNS}

EOF

print "[+] Configuration des utilisateurs locaux d'Asterisk"

cat <<-'EOF' >/etc/asterisk/users.conf
[general]
hasvoicemail = yes             ; Les utilisateurs posséderont une boite vocale
hassip = yes                   ; L’utilisateur possède un compte SIP
hasiax = yes                   ; L’utilisateur possède un compte IAX
callwaiting = yes              ; L’utilisateur peut mettre des appels en attente
threewaycalling = yes
callwaitingcallerid = yes
transfer = yes                 ; L’utilisateur peut transférer des appels
canpark = yes                  ; L’utilisateur peut mettre des appels dans le parking
cancallforward = yes
callreturn = yes
callgroup = 1
pickupgroup = 1
nat = yes                      ; L'utilisateur se trouve derrière un NAT
canreinvite = yes
deny = 0.0.0.0/0.0.0.0
permit = 0.0.0.0/0.0.0.0
qualify = yes                  ; Permet le monitoring

[template_local](!)
type = friend                  ; Type d'objet SIP (friend = utilisateur)
host = dynamic                 ; L'utilisateur n'est pas associé à une IP fixe
dtmfmode = rfc2833             ; Mode DTMF
disallow = all                 ; Interdit tous les codecs
allow = ulaw                   ; Autorise le codec ulaw
context = local                ; Nom du contexte pour ce template
secret = youshallnotpass       ; Secret utilisé pour les utilisateurs du template

[2001](template_local)         ; Numéro SIP et template utilisé
fullname = John SMITH
username = john
mailbox = 2001

[2002](template_local)
fullname = Jane SMITH
username = jane
mailbox = 2002

EOF

print "[+] Configuration de la messagerie vocale d'Asterisk"

cat <<-'EOF' >/etc/asterisk/voicemail.conf
; http://www.voip-info.org/wiki/view/Asterisk+config+voicemail.conf

[general]
maxmsg = 100                             ; Nombre max de message sur la boite de messagerie
maxsecs = 0                              ; Durée max d'un message
minsecs = 0                              ; Durée minimum d'un message
maxlogins = 3                            ; Nombre d'essai maximum pour l'authentification d'un utilisateur sur sa boite vocale
review = yes                             ; Permet à l'appelant de réécouter son message avant de le transmettre à la boite de messagerie. Accessible en terminant le message par #
saycid = no                              ; Dicte le numéro de l'appelant avant de jouer le message qu'il a laissé
format = wav49|gsm|wav                   ; Formats utilisés pour enregistrer les messages vocaux (sauvegarde dans les 3 formats)
servermail = asterisk@univ-poitiers.fr   ; Permet d'identifier la source du serveur mail de notification
attach = yes                             ; Permet d'envoyer en pièce jointe un fichier audio comprenant le message vocal
maxsilence = 10                          ; Temps maximum de silence autorisé avant fin d'appel
silencethreshold = 128                   ; Seuil de détection du silence
sendvoicemail = yes                      ; Permet à un utilisateur d'envoyer un message vocal à un autre utilisateur

; Corps du mail
emaildateformat = %A, %d %B %Y a %H:%M:%S
emailsubject = [ASTERIX] Nouveau message dans la boite ${VM_MAILBOX}
emailbody = Bonjour ${VM_NAME},\n\n\tLe numero ${VM_CALLERID} a tente de vous joindre sans succes le ${VM_DATE}.\nCette personne vous a laisse un message de ${VM_DUR} secondes. Vous pouvez le consulter en appelant votre boite vocale.\n\n\tBonne journee !\n\n\t\t\t\t--Asterix\n
pagerfromstring = [Asterix]
pagersubject = Nouveau message vocal
pagerbody = Nouveau message de ${VM_DUR} secondes dans la boite ${VM_MAILBOX} laisse le ${VM_DATE} par ${VM_CALLERID}.

[local_vm]
2001 => 1234, John SMITH, baptiste.moine@etu.univ-poitiers.fr
2002 => 1234, Jane SMITH, baptiste.moine@etu.univ-poitiers.fr

EOF

print "[+] Configuration du plan de numérotation d'Asterisk"

cat <<-'EOF' >/etc/asterisk/extensions.conf
[local]

include => parkedcalls

; Numéro de groupe
exten => 4000,1,Dial(SIP/2001&SIP/2002,10)

; 1er paramètre : nom de la salle
; 2e paramètre  : salle à rejoindre lorsque l'on compose le numéro
; 3e paramètre  : profil d’utilisateur
; 4e paramètre  : menu utilisé
exten => 5000,n,ConfBridge(room1,testbridge,testuser,testmenu)

exten => _2XXX,1,Log(NOTICE, Incoming call from ${CALLERID(all)})
exten => _2XXX,2,Dial(SIP/${EXTEN},10,Tt)
exten => _2XXX,3,Voicemail(${EXTEN}@local_vm)

exten => 600,1,Voicemailmain(${CALLERID(num)}@local_vm)
exten => 601,1,Goto(voicemail-msg,s,1)

; paramètre t : autorise la fonction transfert pour l'appelé
; paramètre T : autorise la fonction transfert pour l'appelant
; paramètre x : autorise la fonction enregistrement pour l'appelé
; paramètre X : autorise la fonction enregistrement pour l'appelant
exten => asterisk-b,1,Dial(SIP/2001,10,TtxX)
exten => asterisk-b,2,Hangup()

exten => _00549511001,1,Dial(SIP/asterisk-a@open-ims.test,20,r)
exten => _00549511001,2,Hangup()

[voicemail-msg]
exten => s,1,Answer
exten => s,2,agi(googletts.agi,"Bienvenus dans l'utilitaire de création de messages daccueil.",fr,any)
exten => s,3,agi(googletts.agi,"Veuillez après le bipe annoncer votre message daccueil et validez avec dièse.",fr,any)
exten => s,4,Record(MSG-${CALLERID(num)}:ulaw)
exten => s,5,agi(googletts.agi,"Voici votre message daccueil: ",fr,any)
exten => s,6,Playback(MSG-${CALLERID(num)})
exten => s,7,agi(googletts.agi,"Si vous souhaitez le ré enregistrer appuyez sur 1",fr,any)
exten => s,8,agi(googletts.agi,"Si vous souhaitez garder ce message vous pouvez raccrocher",fr,any)
exten => s,9,Set(TIMEOUT(response)=10)
exten => s,10,WaitExten()
exten => 1,1,Goto(voicemail-msg,s,3)
exten => _[2-9#],1,Goto(voicemail-msg,s,7)
exten => t,1,Goto(voicemail-msg,s,7)

EOF

print "[+] Configuration de l'enregistrement SIP d'Asterisk sur OpenIMS"

cat <<-EOF >/etc/asterisk/sip.conf
[general]
; http://www.voip-info.org/wiki/view/Asterisk+config+sip.conf

dtmfmode = rfc2833    ; Mode DTMF
bindaddr = ${IP}      ; Adresse d'écoute
bindport = 5060       ; Port d'écoute
language = fr         ; Langage utilisé par Playback et Background
srvlookup = yes       ; Autorise le serveur à utiliser le DNS
canreinvite = yes
defaultexpiry = 3600  ; Durée avant expiration d'un enregistrement
registertimeout = 30  ; Nombre max de secondes attendues avant réponse du registrar sip
registerattempts = 0  ; Nombre max de messages REGISTER envoyés avant abandon
allowguest = yes      ; Accepte les appels invités
nat = yes
context = local
register => asterisk-b@open-ims.test:0000:asterisk-b@open-ims.test@pcscf.open-ims.test:4060/asterisk-b

EOF

print "[+] Rechargement des configurations d'Asterisk"

asterisk -rvvv
reload
sip show registry
sip show peers
