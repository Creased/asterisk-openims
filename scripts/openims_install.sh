#!/usr/bin/env bash
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

function print() {
    if (( $# == 1 )); then
        str=${1}
        printf "%b" "${str}\n" >&3
    fi
}

exec 3>/dev/stdout
exec 2>&1
exec 1>/dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

export HOSTNAME=openims
export DOMAIN=open-ims.test
export IP=192.168.88.115
export MASK=255.255.248.0
export GW=192.168.93.254
export DNS=192.168.92.6
export DEBIAN_FRONTEND=noninteractive
export DB_PASSWD=root
export IP_ASTERISK_A=192.168.88.225
export IP_ASTERISK_B=192.168.88.226

print "
# HOSTNAME:\t\t${HOSTNAME}\t\t\t#
# DOMAIN:\t\t${DOMAIN}\t\t#
# IP:\t\t\t${IP}\t\t#
# MASK:\t\t\t${MASK}\t\t#
# GW:\t\t\t${GW}\t\t#
# DNS:\t\t\t${DNS}\t\t#
# DB_PASSWD:\t\t${DB_PASSWD}\t\t\t#
# DEBIAN_FRONTEND:\t${DEBIAN_FRONTEND}\t\t#
"

print "[+] Configuration réseau"

cat <<-EOF >/etc/network/interfaces
##
# Default configuration
#
iface default inet dhcp

##
# Loopback NIC configuration
#
auto lo
iface lo inet loopback

##
# Ethernet NIC configuration
#
auto eth0
allow-hotplug eth0
iface eth0 inet static
    address ${IP}
    netmask ${MASK}
    gateway ${GW}
    dns-search ${DOMAIN}
    dns-nameservers ${IP_IMS} ${DNS}

EOF

ifdown eth0; ifup eth0

print "[+] Configuration du nom d'hôte"

cat <<EOF >/etc/hostname
${HOSTNAME}
EOF

cat <<EOF >/etc/hosts
# IPv4
127.0.0.1 localhost
127.0.1.1 ${HOSTNAME}

# IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

hostname ${HOSTNAME}
hostname -b ${HOSTNAME}
hostnamectl set-hostname ${HOSTNAME}
sysctl kernel.hostname

print "[+] Configuration d'APT"

cat <<-'EOF' >/etc/apt/sources.list
# Dépôt de base de Debian Jessie
deb http://httpredir.debian.org/debian/ jessie main contrib
deb-src http://httpredir.debian.org/debian/ jessie main contrib

# Mises à jour distribution stable
deb http://httpredir.debian.org/debian/ jessie-updates main
deb-src http://httpredir.debian.org/debian/ jessie-updates main

# Mises à jour vers distribution stable
deb http://httpredir.debian.org/debian/ jessie-backports main
deb-src http://httpredir.debian.org/debian/ jessie-backports main

# Mises à jour de sécurité
deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main
EOF

apt-get update
apt-get -fy upgrade
apt-get -fy dist-upgrade

print "[+] Installation des utilitaires usuels"

apt-get -fy install curl dnsutils git mercurial netcat ntpdate python3.4 resolvconf subversion vim build-essential apt-utils

ntpdate 0.fr.pool.ntp.org

print "[+] Installation de la chaîne de compilation"

apt-get -fy install bison flex
apt-get -fy install make automake autoconf libtool autogen m4
apt-get -fy install binutils binutils binutils-dev gcc gcc-multilib g++ g++-multilib gdb build-essential
apt-get -fy install linux-headers-$(uname -r)

print "[+] Installation des dépendances"

apt-get -fy install ant openjdk-7-jre openjdk-7-jdk node node-mysql libxml2 libxml2-dev libcurl4-gnutls-dev ipsec-tools libmysqld-dev libmysql-java libmysqld-dev mysql-client

debconf-set-selections <<< "mysql-server mysql-server/root_password password ${DB_PASSWD}"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${DB_PASSWD}"
apt-get -y install mysql-server

print "[+] Installation de FHoSS"

cd /opt/
mkdir -p /opt/OpenIMSCore/{FHoSS,ser_ims}/
svn checkout https://svn.code.sf.net/p/openimscore/code/FHoSS/trunk /opt/OpenIMSCore/FHoSS/
svn checkout https://svn.code.sf.net/p/openimscore/code/ser_ims/trunk /opt/OpenIMSCore/ser_ims/

pushd /opt/OpenIMSCore/
pushd /opt/OpenIMSCore/FHoSS/
ant compile deploy 2>/dev/null
popd

print "[+] Installation d'OpenIMS"

pushd /opt/OpenIMSCore/ser_ims/
make install-libs all 2>/dev/null
popd

print "[+] Importation des données dans la base"

mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/FHoSS/scripts/hss_db.sql
sed -i 's/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/' /opt/OpenIMSCore/FHoSS/scripts/userdata.sql
mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/FHoSS/scripts/userdata.sql
mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/ser_ims/cfg/icscf.sql

print "[+] Préparation d'OpenIMS"

cp /opt/OpenIMSCore/ser_ims/cfg/*.cfg /opt/OpenIMSCore/
cp /opt/OpenIMSCore/ser_ims/cfg/*.xml /opt/OpenIMSCore/
cp /opt/OpenIMSCore/ser_ims/cfg/*.sh /opt/OpenIMSCore/

cat <<-'EOF' >>~/.bash_profile
# Répertoire d’accès à Java
JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64

EOF
source ~/.bash_profile

print "[+] Mise à jour des paramètres d'écoute des services IMS"

sed -i 's/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/' /opt/OpenIMSCore/*.xml
sed -i '
    s/^\(listen=\).*$/\1'${IP}'/;
    s/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/
' /opt/OpenIMSCore/*.cfg
sed -i '
    s/#*\(modparam("scscf","registration_default_algorithm","AKAv1-MD5")\)/#\1/;
    s/#*\(modparam("scscf","registration_default_algorithm","MD5")\)/\1/
' /opt/OpenIMSCore/scscf.cfg

print "[+] Configuration de la base de données HSS"

sed -i 's/^\(host=\).*$/\1'${IP}'/;s/^\(port=\).*$/\18080/' /opt/OpenIMSCore/FHoSS/deploy/hss.properties

print "[+] Installation de screen"

apt-get -fy install screen

print "[+] Exécution des services IMS"

screen -S pcscf -dm bash -c "/opt/OpenIMSCore/pcscf.sh"
screen -S icscf -dm bash -c "/opt/OpenIMSCore/icscf.sh"
screen -S scscf -dm bash -c "/opt/OpenIMSCore/scscf.sh"
screen -S hss -dm bash -c "cd /opt/OpenIMSCore/FHoSS/deploy/; export JAVA_HOME=/usr; /opt/OpenIMSCore/FHoSS/deploy/startup.sh"

screen -ls >&3

print "
# http://${IP}:8080/
# Utilisateur par défaut : hssAdmin
# Mot de passe par défaut : hss
"

print "[+] Installation du DNS Bind9"

apt-get -fy install bind9

systemctl stop bind9.service

print "[+] Installation des zones DNS"

cat <<-'EOF' >/etc/bind/named.conf.local
zone "open-ims.test" {
    type master;
    file "/etc/bind/zones/open-ims.dnszone";
};

EOF

install -m 2775 -o root -g bind -d /etc/bind/zones/
cp /{opt/OpenIMSCore/ser_ims/cfg,etc/bind/zones}/open-ims.dnszone

print "[+] Mise à jour des enregistrements de zone DNS"

sed -i 's/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/' /etc/bind/zones/open-ims.dnszone
cat <<-EOF >>/etc/bind/zones/open-ims.dnszone
asterisk-a              1D IN A         ${IP_ASTERISK_A}
_sip.asterisk-a         1D SRV 0 0 5060 asterisk-a
_sip._udp.asterisk-a    1D SRV 0 0 5060 asterisk-a
_sip._tcp.asterisk-a    1D SRV 0 0 5060 asterisk-a

asterisk-b              1D IN A         ${IP_ASTERISK_B}
_sip.asterisk-b         1D SRV 0 0 5060 asterisk-b
_sip._udp.asterisk-b    1D SRV 0 0 5060 asterisk-b
_sip._tcp.asterisk-b    1D SRV 0 0 5060 asterisk-b

EOF

print "[+] Vérification des zones DNS"

named-checkconf -z /etc/bind/named.conf >&3

systemctl restart bind9.service

print "[+] Configuration de l'hôte"

cat <<-EOF >/etc/resolv.conf
search open-ims.test
domain open-ims.test
nameserver ${IP}
nameserver ${DNS}

EOF

cp /etc/resolv.conf{,.ims}

print "[+] Ajout d'utilisateurs"

/opt/OpenIMSCore/ser_ims/cfg/add-imscore-user_newdb.sh -u asterisk-a -p 0000
sed -i '/^--.*/d' add-user-asterisk-a.sql
mysql -u root -p${DB_PASSWD} <./add-user-asterisk-a.sql

/opt/OpenIMSCore/ser_ims/cfg/add-imscore-user_newdb.sh -u asterisk-b -p 0000
sed -i '/^--.*/d' add-user-asterisk-b.sql
mysql -u root -p${DB_PASSWD} <./add-user-asterisk-b.sql

mysql -u root -p${DB_PASSWD} <<< 'SELECT * FROM hss_db.imsu;' >&3
