#!/usr/bin/env bash
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

kill $(screen -ls | sed -n 's/\(\s\|\t\)\([0-9]*\)\..*/\2/p' | sed ':a;N;$!ba;s/\n/ /g')
fuser -k -n tcp 3868 3869 3870 4060 5060 6060 8080
screen -S pcscf -dm bash -c "/opt/OpenIMSCore/pcscf.sh"
screen -S icscf -dm bash -c "/opt/OpenIMSCore/icscf.sh"
screen -S scscf -dm bash -c "/opt/OpenIMSCore/scscf.sh"
screen -S hss -dm bash -c "cd /opt/OpenIMSCore/FHoSS/deploy/; export JAVA_HOME=/usr; /opt/OpenIMSCore/FHoSS/deploy/startup.sh"
screen -ls
