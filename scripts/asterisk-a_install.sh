#!/usr/bin/env bash
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

function print() {
    if (( $# == 1 )); then
        str=${1}
        printf "%b" "${str}\n" >&3
    fi
}

exec 3>/dev/stdout
exec 2>&1
exec 1>/dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

export HOSTNAME=asterisk-a
export DOMAIN=open-ims.test
export IP=192.168.88.225
export MASK=255.255.248.0
export GW=192.168.93.254
export DNS=192.168.92.6
export DEBIAN_FRONTEND=noninteractive
export IP_IMS=192.168.88.115

print "
# HOSTNAME:\t\t${HOSTNAME}\t\t#
# DOMAIN:\t\t${DOMAIN}\t\t#
# IP:\t\t\t${IP}\t\t#
# MASK:\t\t\t${MASK}\t\t#
# GW:\t\t\t${GW}\t\t#
# DNS:\t\t\t${DNS}\t\t#
# IP_IMS:\t\t${IP_IMS}\t\t#
# DEBIAN_FRONTEND:\t${DEBIAN_FRONTEND}\t\t#
"

print "[+] Configuration réseau"

cat <<-EOF >/etc/network/interfaces
##
# Default configuration
#
iface default inet dhcp

##
# Loopback NIC configuration
#
auto lo
iface lo inet loopback

##
# Ethernet NIC configuration
#
auto eth0
allow-hotplug eth0
iface eth0 inet static
    address ${IP}
    netmask ${MASK}
    gateway ${GW}
    dns-search ${DOMAIN}
    dns-nameservers ${IP_IMS} ${DNS}

EOF

ifdown eth0; ifup eth0

print "[+] Configuration du nom d'hôte"

cat <<EOF >/etc/hostname
${HOSTNAME}
EOF

cat <<EOF >/etc/hosts
# IPv4
127.0.0.1 localhost
127.0.1.1 ${HOSTNAME}

# IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

hostname ${HOSTNAME}
hostname -b ${HOSTNAME}
hostnamectl set-hostname ${HOSTNAME}
sysctl kernel.hostname

print "[+] Configuration de la résolution de noms"

cat <<-EOF >/etc/resolv.conf
search open-ims.test
domain open-ims.test
nameserver ${IP_IMS}
nameserver ${DNS}

EOF

print "[+] Installation des utilitaires usuels"

apt-get -fy install curl dnsutils git mercurial netcat ntpdate python3.4 resolvconf subversion vim build-essential apt-utils

print "[+] Installation de la chaîne de compilation"

apt-get update
apt-get -fy install bison flex
apt-get -fy install make automake autoconf libtool autogen m4
apt-get -fy install binutils binutils-dev gcc gcc-multilib g++ g++-multilib gdb build-essential
apt-get -fy install linux-headers-$(uname -r)

print "[+] Installation des dépendances"

apt-get -fy install libncurses5 libncurses5-dev uuid-dev libjansson-dev libxml2-dev libsqlite3-dev libssl-dev libsrtp0-dev libspeex-dev libspeexdsp-dev libgsm1-dev libxslt1-dev libcurl4-openssl-dev

print "[+] Installation d'Asterisk"

export ASTERISK_VERSION=14
cd /opt/
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-${ASTERISK_VERSION}-current.tar.gz
tar xvzf asterisk-${ASTERISK_VERSION}-current.tar.gz
pushd asterisk-${ASTERISK_VERSION}*/
./configure
make menuselect
# Channel Drivers > Cocher chan_sip
# Escape pour quitter ce menu
# Core Sound Packages > Cocher CORE-SOUNDS-FR-ULAW
# Escape pour quitter ce menu
# Music On Hold File Packages > Décocher MOH-OPSOUND-WAV
#                             > Cocher MOH-OPSOUND-ULAW
# Escape pour quitter ce menu
# Extras Sound Packages > Cocher EXTRA-SOUNDS-FR-ULAW
# Escape pour quitter ce menu
# Sauvegarder avec S
make && make install
make samples
make config
popd

print "[+] Installation de DAHDI"

wget http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-current.tar.gz
tar xvzf dahdi-linux-complete-current.tar.gz
pushd dahdi-linux-complete-*/
make
make install
make config
popd

print "[+] Installation de la suite audio"

apt-get -fy install perl libwww-perl sox mpg123 libsox-fmt-mp3 libpulse0 libmp3lame0 libice6 libasyncns0 libsm6 libmad0 libid3tag0 libtwolame0 pulseaudio

cd /var/lib/asterisk/agi-bin/
wget https://raw.github.com/zaf/asterisk-googletts/master/googletts.agi
chmod +x googletts.agi

print "[+] Configuration de Asterisk"

cp /etc/asterisk/sip.conf{,.bak}
cp /etc/asterisk/users.conf{,.bak}
cp /etc/asterisk/extensions.conf{,.bak}
cp /etc/asterisk/voicemail.conf{,.bak}
cp /etc/asterisk/confbridge.conf{,.bak}

sed -i 's#;\(language=\)en#\1fr#' /etc/asterisk/sip.conf

print "[+] Activation du module de conférence d'Asterisk"

systemctl start asterisk.service
asterisk -rvvv
module show like app_confbridge.so
module load app_confbridge.so
exit

print "[+] Installation de Postfix"

debconf-set-selections <<< "postfix postfix/main_mailer_type select Internet with smarthost"
debconf-set-selections <<< "postfix postfix/mailname string asterisk@univ-poitiers.fr"
debconf-set-selections <<< "postfix postfix/relayhost string smtp.univ-poitiers.fr"
apt-get -y install postfix

print "[+] Configuration des utilisateurs locaux d'Asterisk"

cat <<-'EOF' >/etc/asterisk/users.conf
[general]
hasvoicemail = yes             ; Les utilisateurs posséderont une boite vocale
hassip = yes                   ; L’utilisateur possède un compte SIP
hasiax = yes                   ; L’utilisateur possède un compte IAX
callwaiting = yes              ; L’utilisateur peut mettre des appels en attente
threewaycalling = yes
callwaitingcallerid = yes
transfer = yes                 ; L’utilisateur peut transférer des appels
canpark = yes                  ; L’utilisateur peut mettre des appels dans le parking
cancallforward = yes
callreturn = yes
callgroup = 1
pickupgroup = 1
nat = yes                      ; L'utilisateur se trouve derrière un NAT
canreinvite = yes
deny = 0.0.0.0/0.0.0.0
permit = 0.0.0.0/0.0.0.0
qualify = yes                  ; Permet le monitoring

[template_local](!)
type = friend                  ; Type d'objet SIP (friend = utilisateur)
host = dynamic                 ; L'utilisateur n'est pas associé à une IP fixe
dtmfmode = rfc2833             ; Mode DTMF
disallow = all                 ; Interdit tous les codecs
allow = ulaw                   ; Autorise le codec ulaw
context = local                ; Nom du contexte pour ce template
secret = youshallnotpass       ; Secret utilisé pour les utilisateurs du template

[1001](template_local)         ; Numéro SIP et template utilisé
fullname = John DOE
username = john
mailbox = 1001

[1002](template_local)
fullname = Jane DOE
username = jane
mailbox = 1002

EOF

print "[+] Configuration de la messagerie vocale d'Asterisk"

cat <<-'EOF' >/etc/asterisk/voicemail.conf
; http://www.voip-info.org/wiki/view/Asterisk+config+voicemail.conf

[general]
maxmsg = 100                             ; Nombre max de message sur la boite de messagerie
maxsecs = 0                              ; Durée max d'un message
minsecs = 0                              ; Durée minimum d'un message
maxlogins = 3                            ; Nombre d'essai maximum pour l'authentification d'un utilisateur sur sa boite vocale
review = yes                             ; Permet à l'appelant de réécouter son message avant de le transmettre à la boite de messagerie. Accessible en terminant le message par #
saycid = no                              ; Dicte le numéro de l'appelant avant de jouer le message qu'il a laissé
format = wav49|gsm|wav                   ; Formats utilisés pour enregistrer les messages vocaux (sauvegarde dans les 3 formats)
servermail = asterisk@univ-poitiers.fr   ; Permet d'identifier la source du serveur mail de notification
attach = yes                             ; Permet d'envoyer en pièce jointe un fichier audio comprenant le message vocal
maxsilence = 10                          ; Temps maximum de silence autorisé avant fin d'appel
silencethreshold = 128                   ; Seuil de détection du silence
sendvoicemail = yes                      ; Permet à un utilisateur d'envoyer un message vocal à un autre utilisateur

; Corps du mail
emaildateformat = %A, %d %B %Y a %H:%M:%S
emailsubject = [ASTERIX] Nouveau message dans la boite ${VM_MAILBOX}
emailbody = Bonjour ${VM_NAME},\n\n\tLe numero ${VM_CALLERID} a tente de vous joindre sans succes le ${VM_DATE}.\nCette personne vous a laisse un message de ${VM_DUR} secondes. Vous pouvez le consulter en appelant votre boite vocale.\n\n\tBonne journee !\n\n\t\t\t\t--Asterix\n
pagerfromstring = [Asterix]
pagersubject = Nouveau message vocal
pagerbody = Nouveau message de ${VM_DUR} secondes dans la boite ${VM_MAILBOX} laisse le ${VM_DATE} par ${VM_CALLERID}.

[local_vm]
1001 => 1234, John DOE, baptiste.moine@etu.univ-poitiers.fr
1002 => 1234, Jane DOE, baptiste.moine@etu.univ-poitiers.fr

EOF

print "[+] Configuration du plan de numérotation d'Asterisk"

cat <<-'EOF' >/etc/asterisk/extensions.conf
[local]

include => parkedcalls

; Numéro de groupe
exten => 4000,1,Dial(SIP/1001&SIP/1002,10)

; 1er paramètre : nom de la salle
; 2e paramètre  : salle à rejoindre lorsque l'on compose le numéro
; 3e paramètre  : profil d’utilisateur
; 4e paramètre  : menu utilisé
exten => 5000,n,ConfBridge(room1,testbridge,testuser,testmenu)

exten => _1XXX,1,Log(NOTICE, Incoming call from ${CALLERID(all)})
exten => _1XXX,2,Dial(SIP/${EXTEN},10,Tt)
exten => _1XXX,3,Voicemail(${EXTEN}@local_vm)

exten => 600,1,Voicemailmain(${CALLERID(num)}@local_vm)
exten => 601,1,Goto(voicemail-msg,s,1)

; paramètre t : autorise la fonction transfert pour l'appelé
; paramètre T : autorise la fonction transfert pour l'appelant
; paramètre x : autorise la fonction enregistrement pour l'appelé
; paramètre X : autorise la fonction enregistrement pour l'appelant
exten => asterisk-a,1,Dial(SIP/1001,10,TtxX)
exten => asterisk-a,2,Hangup()

exten => _00549512001,1,Dial(SIP/asterisk-b@open-ims.test,20,r)
exten => _00549512001,2,Hangup()

[voicemail-msg]
exten => s,1,Answer
exten => s,2,agi(googletts.agi,"Bienvenus dans l'utilitaire de création de messages daccueil.",fr,any)
exten => s,3,agi(googletts.agi,"Veuillez après le bipe annoncer votre message daccueil et validez avec dièse.",fr,any)
exten => s,4,Record(MSG-${CALLERID(num)}:ulaw)
exten => s,5,agi(googletts.agi,"Voici votre message daccueil: ",fr,any)
exten => s,6,Playback(MSG-${CALLERID(num)})
exten => s,7,agi(googletts.agi,"Si vous souhaitez le ré enregistrer appuyez sur 1",fr,any)
exten => s,8,agi(googletts.agi,"Si vous souhaitez garder ce message vous pouvez raccrocher",fr,any)
exten => s,9,Set(TIMEOUT(response)=10)
exten => s,10,WaitExten()
exten => 1,1,Goto(voicemail-msg,s,3)
exten => _[2-9#],1,Goto(voicemail-msg,s,7)
exten => t,1,Goto(voicemail-msg,s,7)

EOF

print "[+] Configuration du module de conférence d'Asterisk"

cat <<-EOF >/etc/asterisk/confbridge.conf
[testbridge]
type=bridge
max_members=20
mixing_interval=10
internal_sample_rate=auto
record_conference=no
video_mode=follow_talker

[testuser]
type=user
music_on_hold_when_empty=yes
music_on_hold_class=default
announce_user_count_all=yes
announce_join_leave=yes
dsp_drop_silence=yes
denoise=yes

[testmenu]
type=menu
*=playback_and_continue(conf-usermenu)
*1=toggle_mute
1=toggle_mute
*4=decrease_listening_volume
4=decrease_listening_volume
*6=increase_listening_volume
6=increase_listening_volume
*7=decrease_talking_volume
7=decrease_talking_volume
*8=leave_conference
8=leave_conference

EOF

print "[+] Configuration de l'enregistrement SIP d'Asterisk sur OpenIMS"

cat <<-EOF >/etc/asterisk/sip.conf
[general]
; http://www.voip-info.org/wiki/view/Asterisk+config+sip.conf

dtmfmode = rfc2833    ; Mode DTMF
bindaddr = ${IP}      ; Adresse d'écoute
bindport = 5060       ; Port d'écoute
language = fr         ; Langage utilisé par Playback et Background
srvlookup = yes       ; Autorise le serveur à utiliser le DNS
canreinvite = yes
defaultexpiry = 3600  ; Durée avant expiration d'un enregistrement
registertimeout = 30  ; Nombre max de secondes attendues avant réponse du registrar sip
registerattempts = 0  ; Nombre max de messages REGISTER envoyés avant abandon
allowguest = yes      ; Accepte les appels invités
nat = yes
context = local
register => asterisk-a@open-ims.test:0000:asterisk-a@open-ims.test@pcscf.open-ims.test:4060/asterisk-a

EOF

print "[+] Configuration du parcage et transfert d'appels dans Asterisk"

sed -i '{
    s/;*\(blindxfer => \)[^ ]*\(.*\)/\1#1\2/;  # Transfert aveugle
    s/;*\(atxfer => \)[^ ]*\(.*\)/\1*2\2/;     # Transfert sur un numéro composé
    s/;*\(pickupexten = \)[^ ]*\(.*\)/\1*8\2/; # Interruption d un appel sur le reseau
    s/;*\(parkcall => \)[^ ]*\(.*\)/\1#72\2/   # Met un utilisateur sur le parking
}' /etc/asterisk/features.conf

sed -i '{
    s/;*\(parkext => \)[^ ]*\(.*\)/\1700\2/;         # Extension pour le parcage
    s/;*\(parkpos => \)[^ ]*\(.*\)/\1701-720\2/;     # Plage de stockage sur le parking
    s/;*\(context => \)[^ ]*\(.*\)/\1parkedcalls\2/  # Contexte associé au parking
}' /etc/asterisk/res_parking.conf

print "[+] Rechargement des configurations d'Asterisk"

asterisk -rvvv
reload
sip show registry
sip show peers
